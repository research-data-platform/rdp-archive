<?php
/**
 * @file
 * Defines class CdstarFileRepository.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarFileRepository
 */
class CdstarFileRepository {

  /**
   * Save the given CdstarFile within the associated CDSTAR storage object.
   *
   * @param CdstarFile $cdstarfile
   *
   * @return bool True if the file was stored successfully.
   */
  public static function save(CdstarFile $cdstarfile) {
    $object = CdstarObjectRepository::findById($cdstarfile->getObject());
    $server = CdstarServerRepository::findById($object->getServer());

    $request = CdstarAPI::saveFile($server, $cdstarfile);

    $result = json_decode($request['result']);

    if ($request['validation']) {

      // set the (normalized/hashed) filename under which
      // CDSTAR service stored the file as id (and other attributes)
      self::appendResultData($cdstarfile, $result);

      // append metadata
      CdstarAPI::saveFileMetadata($server, $cdstarfile);

      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Delete the given CdstarFile from the associated CDSTAR storage object.
   *
   * @param CdstarFile $cdstarfile
   *
   * @return bool True if the file was deleted successfully.
   */
  public static function delete(CdstarFile $cdstarfile) {
    $request = CdstarAPI::deleteFile($cdstarfile);

    return $request['validation'];
  }

  /**
   * Fetch file from CDSTAR service, trigger automatic file download to client
   * browser. The file is not cached at the Drupal web server but streamed
   * directly from CDSTAR server to the client.
   *
   * @param CdstarFile $cdstarfile
   */
  public static function download(CdstarFile $cdstarfile) {
    try {
      $object = CdstarObjectRepository::findById($cdstarfile->getObject());
      $server = CdstarServerRepository::findById($object->getServer());

      // Set HTTP header for instant file download
      header('Content-Disposition: attachment; filename="' . $cdstarfile->getFilename() . '"');
      header('Content-Type: ' . $cdstarfile->getContentType());
      // Pass HTTP response to browser
      CdstarAPI::downloadFile($server, $object->getId(), $cdstarfile, TRUE);

    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  public static function getBinaryFile(CdStarFile $cdstarfile){
    try{
      $object = CdstarObjectRepository::findById($cdstarfile->getObject());
      $server = CdstarServerRepository::findById($object->getServer());

      return CdstarAPI::downloadFile($server, $object->getId(), $cdstarfile, FALSE);

    } catch (Exception $e){
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  /**
   * For a given CdstarFile instance (first parameter), set attributes from
   * the result of a JSON-decoded CDSTAR API response.
   *
   * @param \CdstarFile $cdstarfile The \CdstarFile instance to update.
   * @param \stdClass $result Results from CDSTAR API query
   */
  public static function appendResultData(CdstarFile $cdstarfile, $result) {
    $cdstarfile->setId($result->id);
    $cdstarfile->setFilename($result->name);

    // retrieve original filename from metadata
    if (isset($result->meta->{"dc:title"}[0])) {
      $cdstarfile->setFilename($result->meta->{"dc:title"}[0]);
    }

    $cdstarfile->setCreated(strtotime($result->created));
    $cdstarfile->setFilesize($result->size);
    $checksum_type = 'md5';
    $cdstarfile->setChecksum($result->digests->$checksum_type);
    $cdstarfile->setChecksumAlgorithm($checksum_type);
    $cdstarfile->setContentType($result->type);
    $cdstarfile->setLastModified(strtotime($result->modified));
  }
}
