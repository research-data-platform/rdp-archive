<?php
/**
 * @file
 * Defines class CdstarObject.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarObject
 */
class CdstarObject {

  /**
   * Constant identifier for unsaved objects.
   */
  const EMPTY_ID = -1;

  const DATE_FORMAT = DATE_RFC3339;

  /**
   * @var string Unique Identifier assigned to the object by CDSTAR server
   */
  private $id = self::EMPTY_ID;

  /**
   * @var int CdstarServer::id of the corresponding server configuration
   */
  private $server;

  /**
   * @var string A manually set display name for the object.
   */
  private $display_name;

  /**
   * @var int Drupal id of the user that created the object
   */
  private $owner;

  /**
   * @var string JSON encoded array
   */
  private $metadata;

  /**
   * @var string JSON encoded array
   */
  private $permissions;

  /**
   * @var int $revision revision number (attribute managed by CDSTAR service)
   */
  private $revision;

  /**
   * @var \DateTime $created object creation timestamp (attribute managed by
   *   CDSTAR service)
   */
  private $created;

  /**
   * @var \DateTime $modified object modification timestamp (attribute managed
   *   by CDSTAR service)
   */
  private $modified;

  /**
   * @var int $file_count number of files stored within the object (attribute
   *   managed by CDSTAR service)
   */
  private $file_count;

  /**
   *
   * @var CdstarFile[] Associative array of CdstarFile instances. File IDs
   *   are array keys, CdstarFile objects the array values
   */
  private $files = [];

  /**
   * CdstarObject constructor.
   */
  public function __construct() {
    $this->created = new DateTime("now", new DateTimeZone(drupal_get_user_timezone()));

    global $user;
    $this->owner = $user->uid;
  }

  public function __toString() {
    return json_encode(get_object_vars($this));
  }

  /**
   * Build table headers compatible with Drupal table theme and
   *
   * @return array Table column headers for simple table display with Drupal
   *   table theme
   * @see \TableSort extension.
   *
   */
  public static function tableHeader() {
    return [
      ['data' => t('CDSTAR Object'), 'field' => 'display_name'],
      ['data' => t('Server'), 'field' => 'server'],
      ['data' => t('Unique Identifier'), 'field' => 'id'],
      ['data' => t('Owner'), 'field' => 'owner'],
      t('Options'),
    ];
  }

  /**
   * Fetch object with a given identifier.
   *
   * @param string $object_id
   *
   * @return CdstarObject
   */
  public static function getObject($object_id) {
    return CdstarObjectRepository::findById($object_id);
  }

  /**
   * Build simple Drupal form to add a file to this CdstarObject instance.
   *
   * @return array Drupal Form array.
   */
  public static function getFormAddFile($submit_handler = 'cdstar_object_addfile_form_submit') {
    $form = [];

    $form['cdstar_file'] = [
      '#type' => 'file',
      '#title' => '<h4>Add new file</h4>',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Upload'),
    ];

    $form['#submit'][] = $submit_handler;

    return $form;
  }

  /**
   * Validator for ZIP-archive upload
   *
   * @param string $upload_fieldname Name of the file upload form field.
   */
  public static function formCreateUploadValidate($upload_fieldname = 'zip_archive') {

    $upload_error = $_FILES['files']['error'][$upload_fieldname];
    $content_type = $_FILES['files']['type'][$upload_fieldname];

    // Test for POST upload errors
    if ($upload_error == 1) {
      // Max upload size exceeded
      form_set_error($upload_fieldname, 'File size exceeds maximum upload
    file size. Please inform your administrator.');
    }
    elseif ($upload_error == 4) {
      // No file uploaded
      form_set_error($upload_fieldname, 'Please choose a file for upload.');
    }

    // Verify content type
    if (!in_array($content_type, CdstarAPI::$archive_types)) {
      form_set_error($upload_fieldname, 'Not a supported archive/compression format.');
    }
  }

  /**
   * @return int
   */
  public function getRevision() {
    return $this->revision;
  }

  /**
   * @param int $revision
   */
  public function setRevision($revision) {
    $this->revision = $revision;
  }

  /**
   * @return \DateTime
   */
  public function getCreated() {
    $this->created->setTimezone(new DateTimeZone(drupal_get_user_timezone()));
    return $this->created;
  }

  /**
   * @param \DateTime $created
   */
  public function setCreated($created) {
    $this->created = $created;
  }

  /**
   * @return \DateTime
   */
  public function getModified() {
    $this->modified->setTimezone(new DateTimeZone(drupal_get_user_timezone()));
    return $this->modified;
  }

  /**
   * @param \DateTime $modified
   */
  public function setModified($modified) {
    $this->modified = $modified;
  }

  /**
   * @return int
   */
  public function getFileCount() {
    return $this->file_count;
  }

  /**
   * @param int $file_count
   */
  public function setFileCount($file_count) {
    $this->file_count = $file_count;
  }

  /**
   * @return array human-readable data fields for simple table row display with
   *   Drupal table theme.
   */
  public function tableRow() {
    $serverlabel = CdstarServerRepository::findById($this->server)->getLabel();
    $ownerlabel = user_load($this->owner)->name;

    $whitespace = '&nbsp;';
    $view_icon = l(CdstarResources::GLYPHICON_VIEW, $this->url(), ['html' => TRUE]);
    $delete_icon = l(CdstarResources::GLYPHICON_DELETE, $this->url('delete'), ['html' => TRUE]);
    $link_icon = l(CdstarResources::GLYPHICON_LINK, $this->url('landing'), ['html' => TRUE]);

    return [
      l($this->display_name, $this->url()),
      $serverlabel,
      $this->id,
      $ownerlabel,
      $view_icon . $whitespace . $link_icon . $whitespace . $delete_icon,
    ];
  }

  /**
   * Build and return different URL paths.
   *
   * @param string $route A string specifying which functional path is desired.
   *
   * @return string
   */
  public function url($route = 'view') {
    switch ($route) {
      case 'view':
        return CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->id;
      case 'delete':
        return CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->id . '/delete';
      case 'landing':
        // CDSTAR server landing page (external link)
        return CdstarServerRepository::findById($this->server)
            ->getUrl() . 'landing/' . $this->id;
    }
  }

  /**
   * @return string HTML representation of associated files.
   */
  public function listFiles() {
    if (count($this->files) == 0) {
      $output = '<p>' . t('No associated files.') . '</p>';
    }
    else {

      $output = '<div class"container-fluid"><div class="row">';
      foreach ($this->files as $key => $value) {
        $output .= $value->htmlDisplay();
      }
      $output .= '</div></div>';
    }
    return $output;
  }

  /**
   * Stores object in repository.
   */
  public function save() {
    $id = CdstarObjectRepository::save($this);

    if ($this->isEmpty()) {
      $this->id = $id;
    }
  }

  /**
   * @return bool
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * Deletes object and attached files from database and CDSTAR storage service.
   *
   * @return boolean true if deleted successfully
   */
  public function delete() {
    return CdstarObjectRepository::delete($this);
  }

  /**
   * Generates the Drupal form field array for a specified class variable (the
   * parameter)
   *
   * @param $fieldname
   *
   * @return array Drupal form field
   */
  public function getFormField($fieldname) {
    switch ($fieldname) {
      case 'display_name':
        return [
          '#type' => 'textfield',
          '#title' => t('Display name'),
          '#description' => t('Enter a display name for the CDSTAR object.
                        The display name can be arbitrary and has no effect on uploaded files.'),
          '#default_value' => $this->display_name,
          '#required' => TRUE,
        ];
      case 'server':
        $servers = CdstarServerRepository::findAll();
        $options = [];
        foreach ($servers as $server) {
          $options[$server->getId()] = $server->getLabel();
        }
        return [
          '#type' => 'select',
          '#title' => t('CDSTAR Server'),
          '#description' => t('Choose a CDSTAR storage service.'),
          '#options' => $options,
          '#default_value' => $this->server,
          '#required' => TRUE,
        ];
    }
  }

  /**
   * Handle Drupal form submit to add a file to this CdstarObject instance.
   *
   * @param string $fieldname
   */
  public function addFileFormAction($fieldname = "cdstar_file") {
    $cdstarfile = new CdstarFile();

    // Pass temporarily-uploaded file data
    $cdstarfile->setFilename($_FILES['files']['name'][$fieldname]);
    $cdstarfile->setContentType($_FILES['files']['type'][$fieldname]);
    if (empty($cdstarfile->getContentType())) {
      $cdstarfile->determine_mime_type();
    }
    $cdstarfile->setTmpfile($_FILES['files']['tmp_name'][$fieldname]);

    $cdstarfile->setObject($this->id);

    if ($cdstarfile->save()) {
      // Add successfully saved file to this object's list.
      $this->addFile($cdstarfile);
    }
  }

  public function addFileFromTus($tus_ID, $filename, $mimetype) {
    $cdstarfile = new CdstarFile();
    $cdstarfile->setFilename($filename);
    $cdstarfile->setContentType($mimetype);
    $cdstarfile->setObject($this->id);

    $server = CdstarServerRepository::findById($this->getServer());
    $file_id = CdstarAPI::addFileFromTus($server, $this->id, $tus_ID, $filename);

    if ($file_id) {
      $cdstarfile->setId($file_id);
      // Add successfully saved file to this object's list.
      $this->addFile($cdstarfile);

      // update cache
      $this->cache();
    }
  }

  /**
   * Add file to private list of attached files.
   * NB: Cache must be updated after execution.
   *
   * @param CdstarFile $cdstarfile
   */
  public function addFile(CdstarFile $cdstarfile) {
    if (!$cdstarfile->isEmpty()) {
      $this->files[$cdstarfile->getId()] = $cdstarfile;
    }
    // update cache
    $this->cache();
  }

  /**
   * Store/update CdstarObject instance in cache.
   */
  private function cache() {
    CdstarObjectRepository::cache($this);
  }

  /**
   * Remove file from private list of attached files.
   * NB: Cache must be updated after execution.
   *
   * @param CdstarFile $cdstarfile
   *
   * @return bool true if file was successfully deleted
   */
  public function removeFile(CdstarFile $cdstarfile) {
    $id = $cdstarfile->getId();
    if (array_key_exists($id, $this->files)) {
      if ($cdstarfile->delete()) {
        unset($this->files[$id]);
        // update cache
        $this->cache();
        return TRUE;
      }
    }

    // Something went wrong...
    return FALSE;
  }

  public function exportZipArchive() {
    try {
      $server = CdstarServerRepository::findById($this->getServer());

      // Append ".zip" file ending if missing in display name
      $filename = strchr($this->getDisplayName(), ".zip") ? $this->getDisplayName() : $this->getDisplayName();

      // Set HTTP header for instant file download
      header('Content-Disposition: attachment; filename="' . $filename . '"');
      header('Content-Type: application/zip');
      // Pass HTTP response to browser
      echo CdstarAPI::exportArchive($server, $this->getId());

    } catch (Exception $e) {
      watchdog_exception('CDSTAR', $e);
      drupal_set_message($e->getMessage(), 'error');
    }
  }

  /************************************* GETTERS AND SETTERS **************************************/

  /**
   * @return CdstarFile[] Associative array of CdstarFile objects:
   *              [CdstarFile::id => CdstarFile]
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param string $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return mixed
   */
  public function getServer() {
    return $this->server;
  }

  /**
   * @param mixed $server
   */
  public function setServer($server) {
    $this->server = $server;
  }

  /**
   * @return mixed
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * @param mixed $metadata
   */
  public function setMetadata($metadata) {
    $this->metadata = is_array($metadata) ? json_encode($metadata) : $metadata;
  }

  /**
   * @return mixed
   */
  public function getOwner() {
    return $this->owner;
  }

  /**
   * @param mixed $owner
   */
  public function setOwner($owner) {
    $this->owner = $owner;
  }

  /**
   * @return string
   */
  public function getDisplayName() {
    return $this->display_name;
  }

  /**
   * @param string $display_name
   */
  public function setDisplayName($display_name) {
    $this->display_name = $display_name;
  }

  /**
   * @return string
   */
  public function getPermissions() {
    return $this->permissions;
  }

  /**
   * @param string $permissions
   */
  public function setPermissions($permissions) {
    $this->permissions = $permissions;
  }

  public function generateBasicMetadata() {
    global $user;
    $metadata = [
      'dc:creator' => [$user->name],
      'dc:created' => [date(self::DATE_FORMAT)],
    ];
    $metadata = json_encode($metadata);
    $this->metadata = $metadata;
  }


}
