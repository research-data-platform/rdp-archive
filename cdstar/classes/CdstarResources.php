<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class CdstarResources {

  const GLYPHICON_VIEW = '<span class="glyphicon glyphicon-eye-open">
                    <span class="sr-only">view</span></span>';

  const GLYPHICON_EDIT = '<span class="glyphicon glyphicon-edit">
                            <span class="sr-only">edit</span></span>';

  const GLYPHICON_LINK = '<span class="glyphicon glyphicon-link">
                            <span class="sr-only">link</span></span>';

  const GLYPHICON_DELETE = '<span class="glyphicon glyphicon-trash" style="color: #FF0000;">
                                <span class="sr-only">delete</span></span>';

  const GLYPHICON_DOWNLOAD = '<span class="glyphicon glyphicon-download">
                                <span class="sr-only">download</span></span>';


}