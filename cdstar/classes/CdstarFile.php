<?php
/**
 * @file
 * Defines class CdstarFile.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarFile
 */
class CdstarFile {

  const EMPTY_ID = -1;

  /**
   * @var string
   */
  private $id = self::EMPTY_ID;

  /**
   * @var string CdstarObject ID
   */
  private $object;

  /**
   * @var string
   */
  private $filename;

  /**
   * @var string
   */
  private $tmpfile;

  /**
   * @var string MIME Type of the file.
   */
  private $content_type;

  /**
   * @var int Filesize in bytes.
   */
  private $filesize;

  /**
   * @var string Algorithmic checksum of the file.
   */
  private $checksum;

  /**
   * @var string The algorithm used to calculate the checksum.
   */
  private $checksum_algorithm;

  /**
   * @var int UTC timestamp of the file creation of the file.
   */
  private $created;

  /**
   * @var int UTC timestamp of the last modification of the file.
   */
  private $last_modified;

  /**
   * @var array of metadata attributes. Keyed by Dublin Core attributes, i.e.:
   * [ 'dc:creator' => 'Max Muster' ]
   */
  private $metadata = [];

  /**
   * Provide a HTML string of the file's attributes including links to download
   * and deletion callbacks.
   *
   * @return string
   * @var bool $with_delete_button If TRUE a button is displayed for deletion of the file
   *
   */
  public function htmlDisplay($with_delete_button = TRUE) {
    $base_route = CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->getObject() . '/';
    return CdstarFileRenderer::display_tile($cdstarFile = $this, $base_route, $with_delete_button);
  }

  /**
   * Save the CdstarFile object to repository.
   *
   * Filename, content type and temporary file pointer must be set.
   *
   * @return bool True if file was successfully saved to repository.
   */
  public function save() {
    // Sanity checks for initial file upload
    if ($this->isEmpty()) {
      // Make sure file name and MIME type are correctly set
      if (empty($this->filename) || empty($this->content_type)) {
        drupal_set_message(t('Filename or Mime-Type not set.'), 'error');
        return FALSE;
      }
      // Make sure temporary uploaded file existist
      if (empty($this->tmpfile)) {
        drupal_set_message(t('Temporary uploaded file not set.'), 'error');
        return FALSE;
      }
    }
    if (CdstarFileRepository::save($this)) {

      // Delete temporary uploaded file pointer
      unset($this->tmpfile);

      return TRUE;
    }
    else {
      return FALSE;
    }
  }

  /**
   * @return bool True if file was deleted successfully from Repository.
   */
  public function delete() {
    return CdstarFileRepository::delete($this);
  }

  /**
   * Triggers file download.
   */
  public function download() {
    CdstarFileRepository::download($this);
  }

  public function getBinaryFileStream() {
    return CdstarFileRepository::getBinaryFile($this);
  }

  /**
   * Checks whether CdstarFile instance is empty.
   * If a instance is empty, that means, that it has not yet been stored into
   * database.
   *
   * @return bool True if empty, i.e. instance has not yet been stored.
   */
  public function isEmpty() {
    if ($this->id == self::EMPTY_ID) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }


  /**
   * Build and return different URL paths.
   *
   * @param string $route A string specifying which functional path is desired.
   *
   * @return string
   */
  public function url($route) {
    switch ($route) {
      case 'delete':
        return CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->object .
          '/' . $this->id . '/delete';
      case 'download':
        return CDSTAR_CONFIG_OBJECT_DEFAULT . '/' . $this->object .
          '/' . $this->id;
    }
  }

  /**
   * @return string
   * @deprecated
   * Replace special characters (Umlaute) and remove spaces from filename.
   *
   */
  public function normalized_filename() {
    // Replace special chars with standard alphanumerical values
    //$filename = unidecode($this->filename);
    // Further remove whitespaces
    //$filename = preg_replace('/[^a-zA-Z0-9.-_]/', '', $filename);

    // calculate a hash value and use the first 12 characters
    $filename = substr(md5($this->filename), 0, 12);

    return $filename;
  }

  /*************************** GETTERS AND SETTERS ****************************/

  /**
   * @param string $id
   */

  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param mixed $filename
   */
  public function setFilename($filename) {
    $this->filename = $filename;
    $this->metadata['dc:title'] = $filename;
  }

  /**
   * @return mixed
   */
  public function getFilename() {
    return $this->filename;
  }

  /**
   * @param mixed $content_type
   */
  public function setContentType($content_type) {
    $this->content_type = $content_type;
    $this->metadata['dc:FileFormat'] = $content_type;
  }

  /**
   * @return mixed
   */
  public function getContentType() {
    return $this->content_type;
  }

  /**
   * @param mixed $tmpfile
   */
  public function setTmpfile($tmpfile) {
    $this->tmpfile = $tmpfile;
  }

  /**
   * @return mixed
   */
  public function getTmpfile() {
    return $this->tmpfile;
  }

  /**
   * @return int CdstarObject::id
   */
  public function getObject() {
    return $this->object;
  }

  /**
   * @param int $object The ID of a CdstarObject
   */
  public function setObject($object_id) {
    $this->object = $object_id;
  }


  /**
   * @return mixed
   */
  public function getFilesize() {
    return $this->filesize;
  }

  /**
   * @param mixed $filesize
   */
  public function setFilesize($filesize) {
    $this->filesize = $filesize;
    $this->metadata['dc:size'] = $filesize;
  }

  /**
   * @return mixed
   */
  public function getChecksum() {
    return $this->checksum;
  }

  /**
   * @param mixed $checksum
   */
  public function setChecksum($checksum) {
    $this->checksum = $checksum;
  }

  /**
   * @return mixed
   */
  public function getCreated() {
    return $this->created;
  }

  /**
   * @param mixed $created
   */
  public function setCreated($created) {
    $this->created = $created;
  }

  /**
   * @return mixed
   */
  public function getChecksumAlgorithm() {
    return $this->checksum_algorithm;
  }

  /**
   * @param mixed $checksum_algorithm
   */
  public function setChecksumAlgorithm($checksum_algorithm) {
    $this->checksum_algorithm = $checksum_algorithm;
  }

  /**
   * @return mixed
   */
  public function getLastModified() {
    return $this->last_modified;
  }

  /**
   * @param mixed $last_modified
   */
  public function setLastModified($last_modified) {
    $this->last_modified = $last_modified;
  }

  /**
   * @return array
   */
  public function getMetadata() {
    return $this->metadata;
  }

  /**
   * @param array $metadata
   */
  public function setMetadata($metadata) {
    $this->metadata = $metadata;
  }

  /**
   * Return MIME type based on file extension,
   * credit: http://www.thecave.info/php-get-mime-type-from-file-extension/
   *
   * @param $filename
   *
   * @return mixed
   */
  public function determine_mime_type() {

    // our list of mime types
    $mime_types = [
      "pdf" => "application/pdf"
      ,
      "exe" => "application/octet-stream"
      ,
      "zip" => "application/zip"
      ,
      "docx" => "application/msword"
      ,
      "doc" => "application/msword"
      ,
      "xls" => "application/vnd.ms-excel"
      ,
      "ppt" => "application/vnd.ms-powerpoint"
      ,
      "gif" => "image/gif"
      ,
      "png" => "image/png"
      ,
      "jpeg" => "image/jpg"
      ,
      "jpg" => "image/jpg"
      ,
      "mp3" => "audio/mpeg"
      ,
      "wav" => "audio/x-wav"
      ,
      "mpeg" => "video/mpeg"
      ,
      "mpg" => "video/mpeg"
      ,
      "mpe" => "video/mpeg"
      ,
      "mov" => "video/quicktime"
      ,
      "avi" => "video/x-msvideo"
      ,
      "3gp" => "video/3gpp"
      ,
      "css" => "text/css"
      ,
      "jsc" => "application/javascript"
      ,
      "js" => "application/javascript"
      ,
      "php" => "text/html"
      ,
      "htm" => "text/html"
      ,
      "html" => "text/html",
    ];

    $filename = $this->getFilename();
    $splittedFilename = explode('.', $filename);
    $extension = strtolower(end($splittedFilename));

    if (array_key_exists($extension, $mime_types)) {
      $this->setContentType($mime_types[$extension]);
    }
  }

}
