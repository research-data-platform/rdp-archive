<?php
/**
 * @file
 * Defines abstract class CdstarAPI.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class CdstarAPI (v3)
 *
 * Encapsulates communication with CDSTAR storage services' REST interface.
 */
abstract class CdstarAPI {

  /**
   * @var array MIME types applicable for
   * archive upload and automatic extraction
   */
  public static $archive_types = [
    'zip' => 'application/zip',
    'tar' => 'application/x-tar',
    'gzip' => 'application/gzip',
    'x-bzip2' => 'application/x-bzip2',
    'xz' => 'application/x-xz',
    'win-zip' => 'application/x-zip-compressed',
    // 'deflate' => '',
  ];

  /**
   * @var array Compression MIME types applicable
   * for archive upload and automatic extraction
   */
  public static $compression_types = [
    'gzip' => 'application/gzip',
    'x-bzip2' => 'application/x-bzip2',
    'xz' => 'application/x-xz',
    // 'deflate' => '',
  ];

  /**
   * @var string[] Static variable, array of HTTP Header strings.
   */
  private static $header = [];

  /**
   * Test the authentication credentials against CDSTAR API URL of a given
   * \CdstarServer instance. Try to create and delete an object.
   *
   * @param \CdstarServer $server
   *
   * @return bool TRUE on success
   */
  public static function testAuthentication(CdstarServer $server) {

    $request = self::createObject($server);
    $result = $request['result'];
    if ($request['validation']) {
      $id = json_decode($result)->id;
      self::deleteObject($server, $id);
      return TRUE;
    }
    else {
      if ($request['code'] == 301) {
        drupal_set_message(t("Server announces a permanent redirect,
        please try to connect to <strong>%s</strong> instead.",
          ['%s' => $request['location']]), "warning");
      }
      return FALSE;
    }
  }

  /**
   * Create new CDSTAR storage object.
   *
   * @param \CdstarServer $server
   *
   * @return array
   */
  public static function createObject(CdstarServer $server) {

    $ch = self::initRequest($server);

    $method = 'POST';
    $posturl = $server->getUrl(); // v3 API

    return self::executeRequest($ch, $posturl, $method, [201]);
  }

  /**
   * Prepare cURL handle and set common options.
   *
   * @param \CdstarServer $server
   * @param string $custom_method Http Request verb, i.e. GET, PUT, UPDATE.
   *   Default: POST.
   *
   * @return resource cURL handle
   */
  private static function initRequest(CdstarServer $server, $custom_method = "") {

    // clear all headers
    self::$header = [];

    $auth_method = $server->getAuthMethod();

    $ch = curl_init();

    // Set correct authentication method according to CdstarServer configuration
    if ($auth_method === CdstarServer::AUTH_BASIC) {
      $userpwd = self::userpass($server);
      curl_setopt($ch, CURLOPT_USERPWD, $userpwd);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    }
    elseif ($auth_method === CdstarServer::AUTH_JWT) {

      // shared secret
      $key = $server->getPassword();

      // build token-array from claims
      $token = [];
      $token['iss'] = $server->getJwtIss();
      // prevent missing or NULL sub claim; use calling user name as default
      if (!$sub = $server->getJwtSub()) {
        global $user;
        // Username or "anonymous" if not logged in
        $sub = property_exists($user, 'name') ? $user->name : 'anonymous';
      }
      $token['sub'] = $sub;

      // Expiration time (one minute from generation)
      $token['exp'] = time() + 60;

      // build JWT Token
      require_once(drupal_get_path('module', 'cdstar') . '/lib/jwt/src/JWT.php');
      $jwt = Firebase\JWT\JWT::encode($token, $key);

      // set JWT authentication header
      self::$header[] = 'Authorization: Bearer ' . $jwt;
    }

    curl_setopt($ch, CURLOPT_VERBOSE, TRUE);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    if (!empty($custom_method)) {
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $custom_method);
    }
    else {
      curl_setopt($ch, CURLOPT_POST, TRUE);
    }

    // Required for development server settings
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    return $ch;
  }

  /**
   * Prepare authentication parameters. (BasicAuth)
   *
   * @param CdstarServer $server
   *
   * @return string
   */
  private static function userpass(CdstarServer $server) {
    return $server->getUsername() . ':' . $server->getPassword();
  }

  /**
   *
   * @param resource $ch
   * cURL handle
   * @param string $posturl
   * Target URL
   * @param string $method
   * HTTP verb
   * @param array $valid_response_codes
   * HTTP response codes that will be regarded as successful operations.
   * @param bool $display_error_message Wheter of not error messages should be display, default is TRUE
   *
   * @return array Associative array providing the response code as key 'code'
   *   and the transmitted payload as 'result'
   */
  private static function executeRequest(
    $ch,
    $posturl,
    $method = 'POST',
    $valid_response_codes = [200, 201],
    $display_error_message = TRUE
  ) {

    curl_setopt($ch, CURLOPT_URL, $posturl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, self::$header);

    // Log the cdstar request
    watchdog('CDSTAR',
      t('Start HTTP :method call to :url',
        [':method' => $method, ':url' => $posturl]),
      [], WATCHDOG_DEBUG);

    $result = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // flush headers
    self::$header = [];

    $return = [
      'code' => $code,
      'result' => $result,
    ];
    if (in_array($code, $valid_response_codes)) {
      $return['validation'] = TRUE;
    }
    elseif ($code == 301) {
      // "301 Moved Permanently" could indicate http-to-https redirect by server
      $return['location'] = curl_getinfo($ch, CURLINFO_REDIRECT_URL);
      $return['validation'] = FALSE;
    }
    else {
      $return['validation'] = FALSE;
      // In case of an error the result should include the (json) error message
      $responseMessage = is_string($result) ? $result : 'No message available';
      watchdog('CDSTAR',
        t('Error: HTTP :method call returned code :code.
            <p><b>Called URL:</b> :url  <br/><b>Response Message:</b> :message </p>',
          [':method' => $method, ':url' => $posturl, ':code' => $code, ':message' => $responseMessage]),
        [], WATCHDOG_ERROR);
      if ($display_error_message) {
        /* ZIP Archives that are created on a Windows machine, can use unknown encoding for the filenames if they contain special characters like äöü etc.
         * If this is the case, cd star cannot handle these files and returns an Error 400 "InvalidImportFormat".
         * For unknown reasons we only get a 400 "Bad Request" from the curl interface. Thats why we display this message to the user without checking the exact
         * reason for the error.
         */
        if($code == 400){
          drupal_set_message(t('An unexpected error occured in communication
         with CDSTAR storage service.<br>
         Please check if you use some special characters (e.g. äöü) for the files IN the ZIP Archive. Removing them or using other tools to create the ZIP Archive might solve this issue.<br>
         Please contact your system administrator if this does not help to solve the issue'), 'error', FALSE);
        } else {
          drupal_set_message(t('An unexpected error occured in communication
         with CDSTAR storage service. Please contact your system administrator'),
            'error', FALSE);
        }
      }
    }

    curl_close($ch);
    return $return;
  }

  /**
   * Delete object from CDSTAR storage service.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   *
   * @return array
   */
  public static function deleteObject(CdstarServer $server, $id) {
    $method = 'DELETE';
    $posturl = $server->getUrl() . $id;

    $ch = self::initRequest($server, $method);

    // accept 404 "not found" response too: object did not exist (anymore)
    $valid_response_codes = [204, 404];

    return self::executeRequest($ch, $posturl, $method, $valid_response_codes);
  }

  /**
   * Create a new object from a compressed file archive.
   *
   * @param \CdstarServer $cdstarServer
   * @param \CdstarFile $cdstarFile
   *
   * @return array
   */
  public static function createObjectFromArchive(CdstarServer $cdstarServer, CdstarFile $cdstarFile) {

    $method = 'POST';
    $ch = self::initRequest($cdstarServer, $method);

    $posturl = $cdstarServer->getUrl();

    // Prepare content type and encoding headers
    $contentType = $cdstarFile->getContentType();
    if (in_array($contentType, self::$compression_types)) {
      // This condition can only apply for TAR archives
      $encoding = array_keys(self::$compression_types, $contentType);
      drupal_set_message("newest" . json_encode($encoding[0]));
      self::$header[] = 'Content-Encoding: ' . $encoding[0];
      self::$header[] = 'Content-Type: application/x-tar';
    }
    else {
      // The cd-star instance only accept application/zip as content-type. Even if the other archive_types are equivalent
      if (in_array($contentType, self::$archive_types)) {
        self::$header[] = 'Content-Type: application/zip';
      }
      else {
        self::$header[] = 'Content-Type: ' . $contentType;
      }
    }

    // append file
    $tmpfile = $cdstarFile->getTmpfile();
    $infile = fopen($tmpfile, 'r');
    curl_setopt($ch, CURLOPT_UPLOAD, 1);
    curl_setopt($ch, CURLOPT_INFILE, $infile);
    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($tmpfile));

    return self::executeRequest($ch, $posturl, $method, [201]);
  }

  /**
   * Get an array of file objects listing the files stored
   * within a given CdstarObject
   *
   * @param \CdstarObject $object
   *
   * @return \CdstarFile[]
   */
  public static function parseFiles(CdstarObject $object) {
    $server = CdstarServerRepository::findById($object->getServer());

    // Get the list of files
    $request = self::readObject($server, $object->getId());
    $result = json_decode($request['result']);
    $result_files = $result->files;

    // prepare CdstarFile instances from the result list
    $files = [];
    foreach ($result_files as $result_file) {
      $file = new CdstarFile();
      $file->setObject($object->getId());

      CdstarFileRepository::appendResultData($file, $result_file);

      $files[] = $file;
    }

    // Return the array of CdstarFile instances
    return $files;
  }

  /**
   * Get existing object from storage service.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   *
   * @return array
   */
  public static function readObject(CdstarServer $server, $id) {
    $method = 'GET';

    // include metadata, permission and file list
    $posturl = $server->getUrl() . $id . '?with=files,meta,acl&limit=1000';

    $ch = self::initRequest($server, $method);

    return self::executeRequest($ch, $posturl, $method);
  }

  /**
   * Append metadata JSON string to existing CDSTAR storage object.
   *
   * @param \CdstarObject $object
   *
   * @return array
   */
  public static function saveMetadata(CdstarObject $object) {

    $method = 'PUT';
    $server = CdstarServerRepository::findById($object->getServer());
    $ch = self::initRequest($server, $method);

    $posturl = $server->getUrl() . $object->getId() . '?meta';

    curl_setopt($ch, CURLOPT_POSTFIELDS, $object->getMetadata());
    self::$header[] = 'Content-Type: application/json';

    return self::executeRequest($ch, $posturl, $method, [204]);
  }

  /**
   * Append file to existing CDSTAR storage object.
   * Works for create and update actions. (Updates file, if the same filename is
   * uploaded again to the same object/archive.
   *
   * @param CdstarServer $server
   * @param CdstarFile $cdstarfile
   *
   * @return array
   */
  public static function saveFile(
    CdstarServer $server,
    CdstarFile $cdstarfile
  ) {
    $method = 'PUT';

    $filename = rawurlencode($cdstarfile->getFilename());
    $posturl = $server->getUrl() . $cdstarfile->getObject() . '/' . $filename;

    $ch = self::initRequest($server, $method);

    // append file
    $tmpfile = $cdstarfile->getTmpfile();
    $infile = fopen($tmpfile, 'r');
    curl_setopt($ch, CURLOPT_UPLOAD, 1);
    curl_setopt($ch, CURLOPT_INFILE, $infile);
    curl_setopt($ch, CURLOPT_INFILESIZE, filesize($tmpfile));

    // append content-type information
    self::$header[] = 'Content-Type: ' . $cdstarfile->getContentType();

    return self::executeRequest($ch, $posturl, $method, [200, 201]);
  }

  public static function saveFileMetadata(
    CdstarServer $server,
    CdstarFile $cdstarFile
  ) {
    $method = 'PUT';

    $posturl = $server->getUrl() . $cdstarFile->getObject() . '/' .
      rawurlencode($cdstarFile->getFilename()) . '?meta';

    $ch = self::initRequest($server, $method);

    $metadata = $cdstarFile->getMetadata();

    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($metadata));
    self::$header[] = 'Content-Type: application/json';

    return self::executeRequest($ch, $posturl, $method, [204]);
  }

  /**
   * Read a specific file from CDSTAR storage. If the request is
   * successful, the binary file content is re-transmitted by the CDSTAR
   * service.
   *
   * If streamOutput is true, the data will be send directly to the client. This
   * terminates the script but prevents output buffering.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   * @param CdstarFile $cdstarfile
   * @param bool $streamOutput - True if the output stream should be sent to the
   * client without any buffering
   *
   * @return mixed Binary stream of file content on success.
   */
  public static function downloadFile(
    CdstarServer $server,
    $id,
    CdstarFile $cdstarfile,
    bool $streamOutput = FALSE
  ) {
    $method = 'GET';

    $ch = self::initRequest($server, $method);

    // Endpoint consumes "filename" as identifier
    // Use rawurlencode() to correctly transfer whitespaces etc.
    $posturl = $server->getUrl() . $id . '/' . rawurlencode($cdstarfile->getFilename());
    curl_setopt($ch, CURLOPT_URL, $posturl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, self::$header);

    // Streams the (binary) output direct to the client
    // The script execution will end after this!
    if($streamOutput){
      // Adds the Content Length to the http header to display it in the download
      header('Content-Length: '.$cdstarfile->getFilesize());

      self::streamOut($ch);
    }

    // Return binary file stream (this will buffer the whole file in the output
    // buffer!)
    return curl_exec($ch);
  }

  /**
   * Streams all received Bits directly back to the client
   * Attention! This function terminates the request execution! Not even any
   * Drupal functions will be called.
   *
   * This will also prevent buffering (php output buffering).
   * @param CurlHandle|resource $ch
   *
   */
  private static function streamOut($ch){
    ob_end_flush();
    curl_setopt($ch, CURLOPT_WRITEFUNCTION, function($curl, $data) {
      echo $data;
      return strlen($data);
    });

    // increase php max_execution_time.
    // NOTE: THE DOWNLOAD MAY FAIL IF THIS TIME IS TO SHORT
    set_time_limit(3600);
    // execute the curl request
    curl_exec($ch);
    curl_close($ch);

    // Probably this wouldn't be necessary
    set_time_limit(30);

    // terminates the php script to prevent drupal sending header because the
    // writefunction already sent data.
    exit();
  }

  /**
   * Export all files from an archive as a ZIP container.
   *
   * @param \CdstarServer $server
   * @param $id
   * @param null $filter
   *
   * @return mixed Binary stream of a ZIP file on success.
   */
  public static function exportArchive(CdstarServer $server, $id, $filter = NULL) {
    $method = 'GET';

    $ch = self::initRequest($server, $method);

    // ToDo: implement handling of filters

    /** may be added to filter files included in the ZIP archive
     */
    $posturl = $server->getUrl() . $id . '?export=zip';
    curl_setopt($ch, CURLOPT_URL, $posturl);


    curl_setopt($ch, CURLOPT_HTTPHEADER, self::$header);

    watchdog(__CLASS__, "CDSTAR Archive Zip Export requested: %call", ['%call' => $posturl], WATCHDOG_DEBUG);

    // Return binary file stream
    return curl_exec($ch);
  }

  /**
   * Read permissions of an existing CDSTAR storage object.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   *
   * @return array
   */
  public static function readPermissions(CdstarServer $server, $id) {
    $method = 'GET';
    $posturl = $server->getUrl() . $id . '?acl';

    $ch = self::initRequest($server, $method);

    return self::executeRequest($ch, $posturl, $method);
  }

  /**
   * Set permissions of an existing CDSTAR storage object.
   *
   * @param CdstarServer $server
   * @param string $id CDSTAR object ID
   * @param string $permissions JSON string of object permissions.
   *
   * @return array
   */
  public static function setPermissions(
    CdstarServer $server,
    $id,
    $permissions
  ) {
    $method = 'PUT';
    $posturl = $server->getUrl() . $id . '?acl';

    $ch = self::initRequest($server, $method);
    // ToDo: implement for v3
    self::$header[] = 'Content-Type: application/json';
    curl_setopt($ch, CURLOPT_HTTPHEADER, self::$header);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $permissions);

    return self::executeRequest($ch, $posturl, $method);
  }

  /**
   * Delete a specific file from CDSTAR storage service object.
   *
   * @param CdstarFile $cdstarfile
   *
   * @return array
   */
  public static function deleteFile(CdstarFile $cdstarfile) {
    $object = CdstarObjectRepository::findById($cdstarfile->getObject());
    $server = CdstarServerRepository::findById($object->getServer());

    $method = 'DELETE';
    $ch = self::initRequest($server, $method);

    $posturl = $server->getUrl() . $object->getId() . '/' .
      rawurlencode($cdstarfile->getFilename());

    return self::executeRequest($ch, $posturl, $method, [204]);
  }

  /**
   * @param $tus_id
   * @param CdstarServer $server
   *
   * @return string|int ID of the successfully fetched file, 0 if something went wrong
   */
  public static function addFileFromTus(CdstarServer $server, $object_id, $tus_id, $filename) {
    $method = 'POST';
    $ch = self::initRequest($server, $method);

    /**
     * CDSTAR "Update Archive" endpoint, see https://cdstar.gwdg.de/docs/dev/#updateArchive
     */
    $filename = rawurlencode($filename);
    $data = 'fetch:/' . $filename . '=tus:' . $tus_id;
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $posturl = $server->getUrl() . $object_id; // . $parameters;

    watchdog('CDSTAR', t('<p> Start Transfer from TUS to cdstar Object </p> <p>Object ID: %object_id<br>TUS ID: %tus_id</p>', ['%object_id' => $object_id, '%tus_id' => $tus_id]), null, WATCHDOG_DEBUG);

    $request = self::executeRequest($ch, $posturl, $method, [200]);

    if ($request['validation']) {
      // try to set file mimetype
      $ch = self::initRequest($server, $method);
      $data = 'type:/' . $filename . '=application/x-autodetect';
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      $update = self::executeRequest($ch, $posturl, $method, [200], FALSE);

      // get file ID
      $request = self::getFileInfo($server, $object_id, $filename);
      $json_result = $request['result'];
      return json_decode($json_result)->id;
    }
    else {
      return 0;
    }
  }

  public static function getFileInfo(CdstarServer $server, $object_id, $filename) {
    $method = 'GET';
    $ch = self::initRequest($server, $method);
    /**
     * CDSTAR "Get file info" endpoint, see https://cdstar.gwdg.de/docs/dev/#getFileInfo
     */
    $posturl = $server->getUrl() . $object_id . '/' . $filename . '?info';
    return self::executeRequest($ch, $posturl, $method, [200]);
  }

  public static function getFileList(CdstarServer $server, $object_id) {
    $method = 'GET';
    $ch = self::initRequest($server, $method);
    /**
     * CDSTAR "Get file list" endpoint, see https://cdstar.gwdg.de/docs/dev/#getFileList
     */
    $posturl = $server->getUrl() . $object_id . '/?files';
    return self::executeRequest($ch, $posturl, $method, [200]);
  }
}
