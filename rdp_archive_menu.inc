<?php

/**
 * Implements @see hook_menu().
 */
function rdp_archive_menu() {

  /**
   * Add public module landing page only when enabled in configuration.
   */
  $landing = variable_get(RDP_ARCHIVE_VAR_LANDING, TRUE);

  if ($landing) {
    $items[RDP_ARCHIVE_URL_ARCHIVE_DEFAULT] = [
      'title' => 'Research Data Archive',
      'description' => 'Research Data Archive',
      'page callback' => 'rdp_archive_home',
      'page arguments' => [],
      'access callback' => TRUE, // public
      'type' => MENU_NORMAL_ITEM,
      'file' => 'includes/rdp_archive.inc',
      'weight' => 0,
    ];
    /**
     * Add the link to main-menu when enabled in configuration.
     */
    $menu = variable_get(RDP_ARCHIVE_VAR_MAIN_MENU, FALSE);
    if ($menu) {
      $items[RDP_ARCHIVE_URL_ARCHIVE_DEFAULT]['menu_name'] = 'main-menu';
    }
  }

  $items[RDP_ARCHIVE_URL_ARCHIVE_VIEW] = [
    'menu_name' => 'navigation',
    'title callback' => 'rdp_archive_archive_view_title',
    'title arguments' => [1],
    'page callback' => 'rdp_archive_archive_view',
    'page arguments' => [1],
    'access callback' => ['rdp_archive_archive_view_access'],
    'access arguments' => [1],
    'type' => MENU_CALLBACK,
    'file' => 'includes/rdp_archive_archive.inc',
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_VIEW] = [
    'title callback' => 'rdp_archive_archive_object_title',
    'title arguments' => [2],
    'page callback' => 'rdp_archive_archive_object_view',
    'page arguments' => [2],
    'access callback' => ['rdp_archive_archive_object_access'],
    'access arguments' => [2],
    'type' => MENU_CALLBACK,
    'file' => 'includes/rdp_archive_archive_object.inc',
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_EDIT] = [
    'title' => 'Edit ArchiveObject',
    'description' => 'Edit ArchiveObject.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['rdp_archive_archive_object_edit', 2],
    'access callback' => ['rdp_archive_archive_object_owner_access'],
    'access arguments' => [2],
    'type' => MENU_CALLBACK,
    'file' => 'includes/rdp_archive_archive_object_edit.inc',
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_DELETE] = [
    'title' => 'Delete',
    'description' => 'Delete object.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['rdp_archive_archive_object_delete', 2],
    'access callback' => ['rdp_archive_archive_object_owner_access'],
    'access arguments' => [2],
    'type' => MENU_CALLBACK,
    'file' => 'includes/rdp_archive_archive_object.inc',
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_REGISTER_PID] = [
    'page callback' => 'rdp_archive_archive_object_pid',
    'page arguments' => [2],
    'access callback' => ['rdp_archive_archive_object_owner_access'],
    'access arguments' => [2],
    'type' => MENU_CALLBACK,
    'file' => 'includes/rdp_archive_archive_object.inc',
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_FILE_DOWNLOAD] = [
    'page callback' => 'rdp_archive_file_download',
    'page arguments' => [2, 3],
    'file' => 'includes/rdp_archive_file.inc',
    'access callback' => ['rdp_archive_archive_object_access'],
    'access arguments' => [2],
    'type' => MENU_CALLBACK,
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_FILE_DELETE] = [
    'page callback' => 'rdp_amp_delete',
    'page arguments' => [2, 3],
    'file' => 'includes/rdp_amp_file.inc',
    'access callback' => ['rdp_archive_archive_object_owner_access'],
    'access arguments' => [2],
    'type' => MENU_CALLBACK,
  ];


  $items[RDP_ARCHIVE_URL_ARCHIVE_EDIT] = [
    'title callback' => 'rdp_archive_archive_view_title',
    'title arguments' => [1],
    'description' => 'Edit Archive.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['rdp_archive_archive_edit', 1],
    'access arguments' => [RDP_ARCHIVE_PERMISSION_CONFIGURATION],
    'type' => MENU_CALLBACK,
    'file' => 'includes/rdp_archive_archive.inc',
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_CREATE] = [
    'page callback' => 'rdp_archive_archive_object_create',
    'page arguments' => [],
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'file' => 'includes/rdp_archive_archive_object_create.inc',
  ];

  $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_UPLOAD] = [
    'title' => 'Upload',
    'description' => 'Upload file(s)',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['rdp_archive_archive_object_upload', 1],
    'access callback' => ['rdp_archive_archive_object_create_access'],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'includes/rdp_archive_archive_object_upload.inc',
  ];

  /**
   * Display the same Upload button at another route
   */
  $items[RDP_ARCHIVE_URL_ARCHIVE_UPLOAD] = $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_UPLOAD];

  /**
   * Suppress ZIP/TAR upload (and subsequent unpacking of files in CDSTAR) if set in configuration
   */
  $default_upload_allowed = variable_get(RDP_ARCHIVE_VAR_ALLOW_DEFAULT_FILE_UPLOAD, FALSE);
  if ($default_upload_allowed) {
    $items[RDP_ARCHIVE_URL_ARCHIVE_OBJECT_CREATE_UPLOAD] = [
      'title' => 'Upload ZIP/TAR archive',
      'description' => 'Upload ZIP or TAR file to create new Object.',
      'page callback' => 'drupal_get_form',
      'page arguments' => ['rdp_archive_archive_object_create_upload', 1],
      'access callback' => ['rdp_archive_archive_object_create_access'],
      'access arguments' => [1],
      'type' => MENU_LOCAL_ACTION,
      'file' => 'includes/rdp_archive_archive_object.inc',
    ];
  }

  $items[RDP_ARCHIVE_URL_USER_ARCHIVES] = [
    'title' => 'Research Data Archive',
    'description' => 'Show my Research Data Archives',
    'page callback' => 'rdp_archive_user',
    'page arguments' => [1],
    'access callback' => 'rdp_archive_user_access',
    'access arguments' => [
      [
        RDP_ARCHIVE_PERMISSION_PERSONAL_ARCHIVE,
        RDP_ARCHIVE_PERMISSION_GROUP_ARCHIVE,
      ],
    ],
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/rdp_archive_user.inc',
  ];

  $items[RDP_ARCHIVE_URL_CONFIG] = [
    'title' => 'Research Data Archive Configuration',
    'description' => 'Research Data Archive configuration options',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['rdp_archive_config'],
    'access arguments' => [RDP_ARCHIVE_PERMISSION_CONFIGURATION],
    'type' => MENU_NORMAL_ITEM,
    'file' => 'includes/rdp_archive_config.inc',
  ];

  $items[RDP_ARCHIVE_URL_CONFIG_DEFAULT] = [
    'title' => 'Configuration',
    'description' => 'Research Data Archive configuration options',
    'access arguments' => [RDP_ARCHIVE_PERMISSION_CONFIGURATION],
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => 0,
  ];

  $items[RDP_ARCHIVE_URL_CONFIG_OVERVIEW] = [
    'title' => 'Archive Overview',
    'description' => 'View and manage existing Archives.',
    'page callback' => 'rdp_archive_config_overview',
    'page arguments' => [],
    'access arguments' => [RDP_ARCHIVE_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_TASK,
    'weight' => 20,
    'file' => 'includes/rdp_archive_config.inc',
  ];

  $items[RDP_ARCHIVE_URL_CONFIG_CREATE_ARCHIVE] = [
    'title' => 'Add new Archive',
    'description' => 'Add a new Archive.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['rdp_archive_archive_create'],
    'access arguments' => [RDP_ARCHIVE_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'includes/rdp_archive_archive.inc',
  ];

  $items[RDP_ARCHIVE_URL_CONFIG_ROLLOUT_ARCHIVES] = [
    'title' => 'Generate multiple Archives',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['rdp_archive_rollout'],
    'access arguments' => [RDP_ARCHIVE_PERMISSION_CONFIGURATION],
    'type' => MENU_LOCAL_ACTION,
    'file' => 'includes/rdp_archive_rollout.inc',
  ];

  $items[RDP_ARCHIVE_URL_AUTOCOMPLETE_USERNAMES] = [
    'page callback' => '_autocomplete_usernames',
    'access arguments' => [RDP_ARCHIVE_PERMISSION_PERSONAL_ARCHIVE],
    'type' => MENU_CALLBACK,
  ];

  return $items;
}