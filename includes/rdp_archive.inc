<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Display a landing page for the Archive module.
 */
function rdp_archive_home() {

  // Set the title for this page
  drupal_set_title('Research Data Archive');

  $out = '';

  if (user_access(RDP_ARCHIVE_PERMISSION_CONFIGURATION)) {
    $out .= l(ArchiveResources::GLYPHICON_CONFIG, RDP_ARCHIVE_URL_CONFIG, ['html' => TRUE]);
  }

  $variables = [
    'path' => rdp_archive_icon_path(),
    'height' => '120px',
    'attributes' => [
      'style' => 'float: right;',
    ],
  ];
  $out .= theme_image($variables);

  // Introduction text
  $intro_text = variable_get(RDP_ARCHIVE_VAR_DEFAULT_INTRO_TEXT, [
    'value' => '',
    'format' => NULL,
  ]);
  $out .= nl2br($intro_text['value']);

  // Public archives
  $archives = ArchiveRepository::findAllBySharingLevel(SharingLevel::PUBLIC_LEVEL);
  if (count($archives)) {
    $out .= '<h2>Public Archives</h2>';
    foreach ($archives as $archive) {
      $out .= '<p>' . $archive->getName() . '</p>';
    }
  }

  // Public objects
  $objects = ArchiveObjectRepository::findAllByContext(ArchiveObject::CONTEXT_DEFAULT);
  if (count($objects)) {
    $header = ArchiveObjectRenderer::tableHeader();
    $rows = [];
    foreach ($objects as $object) {
      if ($object->checkReadPermission()) {
        $renderer = new ArchiveObjectRenderer($object);
        $rows[] = $renderer->tableRow();
      }
    }

    try {
      if (count($rows)) {
        $out .= '<h2>Available Datasets</h2>';
        $out .= theme('table', ['header' => $header, 'rows' => $rows]);
      }
    } catch (Exception $e) {
      watchdog_exception('rdp-archive', $e);
    }
  }

  // Last modified public

  return $out;
}
