<?php

function rdp_archive_archive_object_edit($form = [], &$form_state, $dataset_id) {

  $archiveObject = ArchiveObjectRepository::findById($dataset_id);
  if (!$archiveObject) {
    drupal_not_found();
    exit();
  }

  $renderer = new ArchiveObjectForm($archiveObject);
  $form = $renderer->getEditForm();
  $form_state['dataset_id'] = $dataset_id;

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save metadata'),
  ];

  $form['cancel'] = [
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#submit' => ['rdp_archive_archive_object_edit_cancel'],
    '#limit_validation_errors' => [],
  ];

  return $form;
}

function rdp_archive_archive_object_edit_submit($form, &$form_state) {
  $values = $form_state['values'];
  ArchiveObjectForm::processEditForm($values, $form_state['dataset_id']);

  $form_state['redirect'] = ArchiveObject::url_by_id($form_state['dataset_id']);
}

/**
 * Form 'Cancel' button handler
 *
 * @param $form
 * @param $form_state
 */
function rdp_archive_archive_object_edit_cancel($form, &$form_state) {
  $form_state['redirect'] = ArchiveObject::url_by_id($form_state['dataset_id']);
}

