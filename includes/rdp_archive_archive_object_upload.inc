<?php

function rdp_archive_archive_object_upload($form = [], &$form_state) {

  drupal_set_title("Upload data");
  drupal_set_breadcrumb([l("Home", "/"), l("Research Data Archive", RDP_ARCHIVE_URL_ARCHIVE_DEFAULT)]);

  $archive_id = ArchiveObjectForm::getUserPersonalArchiveID();
  $archive = ArchiveRepository::findById($archive_id);
  $cdstar_server = CdstarServerRepository::findById($archive->getServer());

  //$archive_id = AMPDatasetForm::getUserPersonalArchiveID();
  $form_state['archive_id'] = $archive_id;

  //load form for either TUS or substitute form
  if ($cdstar_server->hasTusEndpoint()) {
    /**
     * TUS form will redirect to .../archive/create route, see file "rdp_archive_archive_object_create.inc"
     *
     * Upload is processed then by @see \ArchiveObjectRepository::processTusUpload()
     */
    $form = $cdstar_server->getTusUploadForm('tus_upload', 'create', TRUE);
  }
  else {
    /**
     * Fallback: regular upload form will be handled by default submit handler below
     */
    $form = ArchiveObjectForm::newUploadForm($archive_id);
  }

  // Adds a short description (or Hint) under the uploader
  $descriptionValue = variable_get(
    RDP_ARCHIVE_VAR_UPLOADER_DESCRIPTION_TEXT, [
    'value'=>'Hint: Espacially for large files, it can take some time to process after the upload is at 100%.
     Please do not leave or reload the page before redirecting!',
    'format' => NULL]
  );
  $descriptionText = nl2br($descriptionValue['value']);

  $form['description'] = [
    '#type' => 'markup',
    '#markup' => '</br>'. $descriptionText,
    ];

  return $form;
}

/**
 * Drupal form handler.
 */
function rdp_archive_archive_object_upload_validate($form, &$form_state) {

  $upload_fieldname = 'file_upload';

  if ($err_id = $_FILES['files']['error'][$upload_fieldname] !== UPLOAD_ERR_OK) {
    switch ($err_id) {
      case UPLOAD_ERR_INI_SIZE:
        $msg = "Maximum file size exceeded.";
        break;
      default:
        $msg = "An error occured during file upload.";
        watchdog("RDP Archive", "PHP File Upload Error ID: " . $err_id, [], WATCHDOG_ERROR);
        break;
    }
    form_set_error($upload_fieldname, $msg);
  }
}

/**
 * Drupal form handler.
 */
function rdp_archive_archive_object_upload_submit($form, &$form_state) {

  $archive_id = $form_state['archive_id'];
  $values = $form_state['values'];

  $dataset_id = ArchiveObjectForm::processUploadForm($archive_id, $values);

  $form_state['redirect'] = ArchiveObject::url_by_id($dataset_id, 'edit');
}
