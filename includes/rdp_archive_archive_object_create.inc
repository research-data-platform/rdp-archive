<?php

function rdp_archive_archive_object_create() {
  // create log entry
  watchdog('CDSTAR', t('TUS Upload Complete. Start Processing'), null, WATCHDOG_DEBUG);

  $archiveObject = ArchiveObjectRepository::processTusUpload();

  if ($archiveObject) {
    drupal_goto($archiveObject->url('edit'));
  }
  else {

    drupal_set_message("An error occurred in communication with the upload server. 
      Please inform the system administrator.", "error");
    drupal_goto(RDP_AMP_URL_DEFAULT);
  }

}
