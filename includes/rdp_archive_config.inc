<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

function rdp_archive_config($form, &$form_state) {

  $form = [];

  /**
   * --------------- Configure default services (CDSTAR / EPIC) ---------------
   */
  $form['services'] = [
    '#type' => 'fieldset',
    '#title' => t('Configure RDP Archive default services'),
  ];

  $default = variable_get(RDP_ARCHIVE_VAR_DEFAULT_CDSTAR, 0);
  $dummy_object = new CdstarObject();
  if ($default) {
    $dummy_object->setServer($default);
  }
  $form['services'][RDP_ARCHIVE_VAR_DEFAULT_CDSTAR] = $dummy_object->getFormField('server');
  $form['services'][RDP_ARCHIVE_VAR_DEFAULT_CDSTAR]['#required'] = FALSE;

  $default = variable_get(RDP_ARCHIVE_VAR_DEFAULT_PID_SERVICE, EpicPidService::EMPTY_ID);
  $dummy_service = new EpicPidService();
  $dummy_service->setId($default);
  $form['services'][RDP_ARCHIVE_VAR_DEFAULT_PID_SERVICE] = $dummy_service->getSelectField();
  $form['services'][RDP_ARCHIVE_VAR_DEFAULT_PID_SERVICE]['#required'] = FALSE;

  $form['services'][RDP_ARCHIVE_VAR_CONFIG_PID_PREFIX] = [
    '#type' => 'textfield',
    '#title' => t('EPIC PID Prefix'),
    '#description' => t('Alphanumerical string defining generated PIDs.'),
    '#default_value' => variable_get(RDP_ARCHIVE_VAR_CONFIG_PID_PREFIX, 'archive_pid_demo_'),
    '#required' => TRUE,
  ];

  $form['services'][RDP_ARCHIVE_VAR_CONFIG_PID_PADDING] = [
    '#type' => 'textfield',
    '#title' => t('PID Padding'),
    '#description' => t('How many leading 0s shall be
        attached to IDs in PID generation?'),
    '#size' => 12,
    '#maxlength' => 12,
    '#element_validate' => ['element_validate_integer'],
    '#default_value' => variable_get(RDP_ARCHIVE_VAR_CONFIG_PID_PADDING, 6),
    '#required' => TRUE,
  ];

  /**
   * -------------------- General settings -----------------------------------
   */
  $form['settings'] = [
    '#type' => 'fieldset',
    '#title' => t('RDP Archive General Settings'),
  ];

  $default = variable_get(RDP_ARCHIVE_VAR_CREATE_ARCHIVE_ON_USER_INSERT, TRUE);
  $form['settings'][RDP_ARCHIVE_VAR_CREATE_ARCHIVE_ON_USER_INSERT] = [
    '#title' => 'Create Personal Archive for each new user',
    '#type' => 'checkbox',
    '#default_value' => $default,
  ];
  $default = variable_get(RDP_ARCHIVE_VAR_ACTIVATE_ARCHIVE_ON_USER_INSERT, FALSE);
  $form['settings'][RDP_ARCHIVE_VAR_ACTIVATE_ARCHIVE_ON_USER_INSERT] = [
    '#title' => 'Activate all newly created Personal Archives',
    '#type' => 'checkbox',
    '#default_value' => $default,
  ];
  $default = variable_get(RDP_ARCHIVE_VAR_ALLOW_DEFAULT_FILE_UPLOAD, FALSE);
  $form['settings'][RDP_ARCHIVE_VAR_ALLOW_DEFAULT_FILE_UPLOAD] = [
    '#title' => 'Allow users to upload files by standard HTML/PHP fields? Deselect to enforce TUS/Uppy JavaScript based upload.',
    '#type' => 'checkbox',
    '#default_value' => $default,
  ];

  /**
   * -------------------- Configure RDP Archive menu items --------------------
   */
  $form['menu'] = [
    '#type' => 'fieldset',
    '#title' => t('RDP Archive menu'),
  ];

  $default = variable_get(RDP_ARCHIVE_VAR_LANDING, TRUE);
  $form['menu'][RDP_ARCHIVE_VAR_LANDING] = [
    '#title' => 'Enable RDP Archive public landing page',
    '#type' => 'checkbox',
    '#default_value' => $default,
  ];

  $default = variable_get(RDP_ARCHIVE_VAR_MAIN_MENU, FALSE);
  $form['menu'][RDP_ARCHIVE_VAR_MAIN_MENU] = [
    '#title' => 'Enable RDP Archive link in Main menu',
    '#type' => 'checkbox',
    '#default_value' => $default,
  ];

  /**
   * ----------------- Configure Uploader Page  -------------------------------
   */
  $form['upload_page'] = [
    '#type' => 'fieldset',
    '#title' => t('RDP Archive Uploader Description Text'),
  ];

  // Adds formatted default description
  $default = variable_get(RDP_ARCHIVE_VAR_UPLOADER_DESCRIPTION_TEXT, [
    'value' => '
The recommended max upload filesize (for a single file) on this server is 4 GB. Larger files might result in errors or unintended system behaviour.
Please contact the system administrator if you plan to upload larger filesets.

Hint: Espacially large big files, it can take some time to process after the upload is at 100%.
Please do not leave or reload the page before redirecting!',
    'format' => NULL,
  ]);

  $form['upload_page'][RDP_ARCHIVE_VAR_UPLOADER_DESCRIPTION_TEXT] = [
    '#title' => 'Uploader Description',
    '#type' => 'text_format',
    '#format' => isset($default['format']) ? $default['format'] : NULL,
    '#default_value' => $default['value'],
    '#description' => 'This text will be displayed under the TUS Uploader on the upload Page (archive/upload)',
  ];


  /**
   * ----------------- Configure module public landing page  -----------------
   */
  $form['intro'] = [
    '#type' => 'fieldset',
    '#title' => t('RDP Archive landing page introductory text'),
  ];

  $default = variable_get(RDP_ARCHIVE_VAR_DEFAULT_INTRO_TEXT, [
    'value' => '',
    'format' => NULL,
  ]);

  $form['intro'][RDP_ARCHIVE_VAR_DEFAULT_INTRO_TEXT] = [
    '#title' => 'Archive Module introduction text',
    '#type' => 'text_format',
    '#format' => isset($default['format']) ? $default['format'] : NULL,
    '#default_value' => $default['value'],
    '#description' => 'This text will be displayed on top of the module’s
    landing page. It is also public visible if no user is logged in.',
  ];

  return system_settings_form($form);
}

function rdp_archive_config_overview() {
  $output = '<h2>Existing Archives</h2>';

  $header = Archive::tableHeader();
  $archives = ArchiveRepository::findAllTableHeader($header);
  $data = [];
  foreach ($archives as $archive) {
    /* @var \Archive $archive */
    $data[] = $archive->tableRow();
  }
  try {
    $output .= theme('table', ['header' => $header, 'rows' => $data]);
    $output .= theme('pager');
  } catch (Exception $e) {
    watchdog_exception('rdp_archive', $e);
  }

  return $output;
}

