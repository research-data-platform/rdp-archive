<?php
/**
 * @file
 * Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

abstract class EpicPidStatus {

  const UNKNOWN = 0;

  const ASSIGNED = 100;

  const POOLED = 500;

  const UNMANAGED = 900;

  public static function translate($status) {
    switch ($status) {
      case self::UNKNOWN:
        return "Unknown";
      case self::ASSIGNED:
        return "Assigned";
      case self::POOLED:
        return "Pooled";
      case self::UNMANAGED:
        return "Unmanaged";
    }
    return $status;
  }

}