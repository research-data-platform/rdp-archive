<?php
/**
 * @file
 * ToDo: Short description for file.
 *
 * Long description for file.
 *
 * @author  Markus Suhr <markus.suhr@med.uni-goettingen.de>
 * @license GPL-3.0 https://www.gnu.org/licenses/gpl-3.0
 *
 * SPDX-License-Identifier: GPL-3.0
 */

/**
 * Class EpicPidServiceRepository
 */
class EpicPidServiceRepository {

  private static $tableName = 'epic_pid_service';

  private static $databaseFields = [
    'id',
    'service_type',
    'name',
    'url',
    'service_prefix',
    'method',
    'prefix',
    'suffix',
    'auth_type',
    'userpass',
    'filepath_private_key',
    'filepath_certificate',
    'resolve_url',
    'is_dummy',
  ];

  public static function save(EpicPidService $epicPidService) {
    if (isset($epicPidService)) {
      if ($epicPidService->isEmpty()) {
        $id = NULL;
      }
      else {
        $id = $epicPidService->getId();
      }

      try {
        db_merge(self::$tableName)
          ->key(['id' => $id])
          ->fields([
              'name' => $epicPidService->getName(),
              'service_type' => $epicPidService->getServiceType(),
              'url' => $epicPidService->getUrl(),
              'service_prefix' => $epicPidService->getServicePrefix(),
              'method' => $epicPidService->getMethod(),
              'prefix' => $epicPidService->getPrefix(),
              'suffix' => $epicPidService->getSuffix(),
              'auth_type' => $epicPidService->getAuthType(),
              'userpass' => $epicPidService->getUserpass(),
              'filepath_private_key' => $epicPidService->getFilepathPrivateKey(),
              'filepath_certificate' => $epicPidService->getFilepathCertificate(),
              'resolve_url' => $epicPidService->getResolveUrl(),
              'is_dummy' => $epicPidService->isDummy(),
            ]
          )->execute();

        return Database::getConnection()->lastInsertId();

      } catch (Exception $e) {

        watchdog_exception('epic_pid', $e);
        drupal_set_message($e->getMessage(), 'error');

        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Translate database result to class instance.
   *
   * @param stdClass $result Generic database result object.
   *
   * @return EpicPidService
   */
  private static function databaseResultToObject($result) {
    $epicPidService = new EpicPidService();

    if (empty($result)) {
      return $epicPidService;
    }

    $epicPidService->setId($result->id);
    $epicPidService->setServiceType($result->service_type);
    $epicPidService->setName($result->name);
    $epicPidService->setUrl($result->url);
    $epicPidService->setServicePrefix($result->service_prefix);
    $epicPidService->setMethod($result->method);
    $epicPidService->setPrefix($result->prefix);
    $epicPidService->setSuffix($result->suffix);
    if (!empty($result->auth_type)) {
      $epicPidService->setAuthType($result->auth_type);
    }
    if (!empty($result->userpass)) {
      $epicPidService->setUserpass($result->userpass);
    }
    if (!empty($result->filepath_private_key)) {
      $epicPidService->setFilepathPrivateKey($result->filepath_private_key);
    }
    if (!empty($result->filepath_certificate)) {
      $epicPidService->setFilepathCertificate($result->filepath_certificate);
    }
    $epicPidService->setResolveUrl($result->resolve_url);
    $epicPidService->setIsDummy($result->is_dummy);

    return $epicPidService;
  }

  /**
   * Translate multiple database results to class instances.
   *
   * @param stdClass[] $results Array of generic database result objects.
   *
   * @return \EpicPidService[] Array of EpicPidService instances.
   */
  private static function databaseResultsToObjects($results) {
    $epicPidServices = [];
    foreach ($results as $result) {
      $epicPidServices[] = self::databaseResultToObject($result);
    }

    return $epicPidServices;
  }

  /**
   * @param int $service_id
   *
   * @return \EpicPidService
   */
  public static function findById($service_id) {

    $result = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('id', $service_id, '=')
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultToObject($result);
  }

  /**
   * @return \EpicPidService[]
   */
  public static function findAll() {
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  public static function findAllByMethod($method) {
    if (!in_array($method, EpicPidService::getMethods())) {
      return FALSE;
    }
    $results = db_select(self::$tableName, 't')
      ->fields('t', self::$databaseFields)
      ->condition('method', $method)
      ->execute()
      ->fetchAll();

    return self::databaseResultsToObjects($results);
  }

  public static function delete(EpicPidService $epicPidService) {
    if ($epicPidService->getId() == EpicPidService::EMPTY_ID) {
      // instance was not save in the first place
      return TRUE;
    }
    else {
      try {
        db_delete(self::$tableName)
          ->condition('id', $epicPidService->getId(), "=")
          ->execute();

        return TRUE;
      } catch (Exception $e) {
        watchdog_exception('epic_pid', $e);
        return FALSE;
      }
    }
  }

}