<?php


class ArchiveObjectForm {

  /**
   * @var \ArchiveObject
   */
  private $archiveObject;

  public function __construct(ArchiveObject $archiveObject) {
    $this->archiveObject = $archiveObject;
  }

  /**
   * Provide an upload form
   *
   * @return array
   */
  public static function newUploadForm($archive_id) {

    $archiveObject = new ArchiveObject();
    $archiveObject->setArchiveId($archive_id);

    $form = $archiveObject->getForm();

    unset($form['display_name']);

    return $form;
  }

  /**
   * @param int $archive_id ID of the Archive in which to store the object.
   * @param array $values Array of values retrieved from Drupal form
   *
   * @return int ID of the created ArchiveObject object
   */
  public static function processUploadForm($archive_id, $values) {

    $upload_fieldname = 'file_upload';
    $display_name = $_FILES['files']['name'][$upload_fieldname];

    $archive = ArchiveRepository::findById($archive_id);

    $archiveObject = new ArchiveObject();
    $archiveObject->setArchiveId($archive_id);
    $archiveObject->setSharingLevel($values['sharing_level']);
    $archiveObject->setContext(ArchiveObject::CONTEXT);

    $cdstarObject = new CdstarObject();
    $cdstarObject->setDisplayName($display_name);
    $cdstarObject->setServer($archive->getServer());
    $cdstarObject->generateBasicMetadata();
    $cdstarObject->save();

    // Upload file
    $cdstarObject->addFileFormAction($upload_fieldname);
    $cdstarObject->save();

    $user_current = User::getCurrent();
    $user_string = $user_current->getFullname();
    $user_string .= (!empty($orcid = $user_current->getOrcid())) ? ', Orcid ID: ' . $orcid : '';

    $archiveObject->setCdstarObjectId($cdstarObject->getId());
    $archiveObject->setCreator($user_string);
    $archiveObject->setPublisher($user_string);
    $archiveObject->save();

    return $archiveObject->getId();
  }

  /**
   * @return int
   */
  public static function getUserPersonalArchiveID() {
    $user = User::getCurrent();
    try {
      $archive_id = ArchiveRepository::getDefaultPersonalArchiveID($user->getUid());
      return $archive_id;
    } catch (Exception $e) {
      watchdog_exception("RDP_AMP", $e);
      drupal_set_message("No personal archive is available for the current user.
      Please contact your administrator", "error");
      drupal_goto(RDP_ARCHIVE_URL_ARCHIVE_DEFAULT);
    }
  }

  public static function processEditForm($values, $dataset_id) {
    $archiveObject = ArchiveObjectRepository::findById($dataset_id);
    $archiveObject->setName($values['name']);
    $archiveObject->setDescription($values['description']);
    $archiveObject->setSharingLevel($values['sharing_level']);
    $archiveObject->setWorkingGroupID($values['working_group']);

    $archiveObject->setCreator($values['creator']);
    $archiveObject->setContributor($values['contributor']);
    $archiveObject->setPublisher($values['publisher']);
    $archiveObject->setRights($values['rights']);
    $archiveObject->setSource($values['source']);
    $archiveObject->setSubject($values['subject']);
    $archiveObject->save();
  }

  /* ------------------------------ Form fields ----------------------------- */
  // @see https://www.drupal.org/docs/7/api/form-api

  public function getEditForm() {

    //
    // prepare new dataset form
    //

    $form['fieldset-general'] = [
      '#type' => 'fieldset',
      '#title' => '<span class="glyphicon glyphicon-info-sign"></span> ' . t('General'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['fieldset-general']['name'] = $this->getFormFieldName();
    $form['fieldset-general']['description'] = $this->getFormFieldDescription();

    $form['fieldset-settings'] = [
      '#type' => 'fieldset',
      '#title' => '<span class="glyphicon glyphicon-wrench"></span> ' . t('Settings'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['fieldset-settings']['working_group'] = $this->getFormFieldWorkingGroup();
    $form['fieldset-settings']['sharing_level'] = $this->getFormFieldSharingLevel();

    $form['fieldset-metadata'] = [
      '#type' => 'fieldset',
      '#title' => '<span class="glyphicon glyphicon-book"></span> ' . t('Metadata'),
      '#description' => '',
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];
    $form['fieldset-metadata']['creator'] = $this->getFormFieldCreator();
    $form['fieldset-metadata']['contributor'] = $this->getFormFieldContributor();
    $form['fieldset-metadata']['publisher'] = $this->getFormFieldPublisher();
    $form['fieldset-metadata']['rights'] = $this->getFormFieldRights();
    $form['fieldset-metadata']['source'] = $this->getFormFieldSource();
    $form['fieldset-metadata']['subject'] = $this->getFormFieldSubject();

    return $form;
  }

  /**
   * Return an associative array with form control data for 'Name'
   * field.
   *
   * @return array
   * @see drupal_get_form()
   *
   */
  public function getFormFieldName($default = '') {

    if (empty($default)) {
      $default = $this->archiveObject->getName();
    }
    return [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $default,
      '#description' => 'Name or title of the dataset',
      '#attributes' => ['placeholder' => t('')],
      '#maxlength' => 255,
    ];
  }

  /**
   * Return an associative array with form control data for 'description' field.
   *
   * @return array
   * @see drupal_get_form()
   */
  public function getFormFieldDescription() {
    return [
      '#type' => 'textarea',
      '#title' => t('Description'),
      '#default_value' => $this->archiveObject->getDescription(),
      '#description' => 'Description of the dataset',
      '#attributes' => ['placeholder' => t('')],
    ];
  }

  private function getFormFieldWorkingGroup() {
    $groups = User::getCurrent()->getUserWorkingGroups();
    $options = [];
    foreach ($groups as $group) {
      $options[$group->getId()] = $group->getName();
    }
    $spec = [
      '#type' => 'select',
      '#title' => t('Working Group'),
      '#description' => t('Specify the (leading) working group by which the uploaded data created.'),
      '#default_value' => $this->archiveObject->getWorkingGroupId(),
      '#options' => $options,
      '#required' => TRUE,
    ];
    return $spec;
  }

  public function getFormFieldSharingLevel() {
    $form_field = SharingLevel::getFormField();

    $replacements = [
      '#description' => 'Change the visibility (sharing) level of this dataset.',
      '#default_value' => $this->archiveObject->getSharingLevel(),
    ];
    return array_replace($form_field, $replacements);
  }

  private function getFormFieldCreator() {
    $spec = self::build_user_select('Creator',
      '[Dublin Core] Creator: the person primarily responsible for making the resource',
      $this->archiveObject->getCreator());

    return $spec;
  }

  private static function build_user_select($title, $description, $default_value) {
    $spec = [
      '#type' => 'textfield',
      '#title' => t($title),
      '#description' => t($description),
      '#default_value' => $default_value,
      '#autocomplete_path' => RDP_ARCHIVE_URL_AUTOCOMPLETE_USERNAMES,
    ];

    return $spec;
  }

  private function getFormFieldContributor() {
    $spec = self::build_user_select('Contributor',
      '[Dublin Core] Contributor: An entity responsible for making contributions to the resource',
      $this->archiveObject->getContributor());

    return $spec;
  }

  private function getFormFieldPublisher() {
    $spec = self::build_user_select('Publisher',
      '[Dublin Core] Publisher: the person responsible for making the resource available',
      $this->archiveObject->getPublisher());

    return $spec;
  }

  private function getFormFieldRights() {
    $link_cc = l('<span class="glyphicon glyphicon-link"></span>Creative Commons Website',
      'https://creativecommons.org/choose/',
      ['html' => TRUE, 'attributes' => ['target' => '_blank']]);
    $link_fodako = l('<span class="glyphicon glyphicon-link"></span>FoDaKo licensing guideline',
      'https://doi.org/10.5281/zenodo.1463156',
      ['html' => TRUE, 'attributes' => ['target' => '_blank']]);
    return [
      '#type' => 'textfield',
      '#title' => t('Use and Access Rights'),
      '#default_value' => $this->archiveObject->getRights(),
      '#description' => '[Dublin Core] Rights: Information about rights held in and over the resource',
      '#attributes' => ['placeholder' => t('e.g. Creative Commons CC-BY-SA 4.0')],
      '#maxlength' => 255,
      '#suffix' => '<div class="small text-muted" style="margin-top: -8px; margin-bottom: 16px;"><span class="glyphicon glyphicon-info-sign"></span>&nbsp;For further information about content licensing
            please refer to the ' . $link_cc . ' or ' . $link_fodako . '.</div>',
    ];
  }

  /**
   * Return an associative array with form control data for 'source' field.
   *
   * @return array
   * @see drupal_get_form()
   */
  public function getFormFieldSource() {
    return [
      '#type' => 'textfield',
      '#title' => t('Source'),
      '#default_value' => $this->archiveObject->getSource(),
      '#description' => 'A related resource from which the described dataset is derived',
      '#attributes' => ['placeholder' => t('')],
    ];
  }

  /**
   * @return array
   * @see drupal_get_form()
   *
   */
  public function getFormFieldSubject() {
    return [
      '#type' => 'textarea',
      '#title' => t('Subject (keywords)'),
      '#description' => t('[Dublin Core] Subject: topic of the dataset, provided as comma separated keywords'),
      '#default_value' => $this->archiveObject->getSubject(),
    ];
  }

}
